from a prompt from a friend doing a talk tomorrow who asked me to consider him mentioning what i'm working on - i've turned on the twitter newsletter function. thinking of using this first "why" letter as the pinned one giving context for this sprawling undertaking.


why
sovereignty - sustainable less volatile near future, accountable, purposeful
crypto - coercion resistant secrets
cardano - the best heart, non immutable anchor



swot -

s
dog-fooding - first person perspective + scale = advantages and disadvantages 

w
pace
distributed/divided/dispersed


o
agility/nimble scale autonomy
current chaos/crisis of confidence
trend leaning towards liberty, sustainable open source (crypto + a.i) ethics/morals
distributed compute + applied pattern recognition a.i


t
corporatocracy 
hw/sw centralisation

